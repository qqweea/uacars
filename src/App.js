import React, { Component, Suspense } from "react";
import { Route, Switch } from "react-router-dom";

import HomePage from "./components/HomePage";
import Prompt from "./components/Prompt";
// import DetailsPage from "./components/DetailsPage";

import "./App.css";
import "./BootstrapGrid.css";
import Loader from "./components/Loader";
const LazyDetailsPage = React.lazy(() => import("./components/DetailsPage"));

class App extends Component {
  deferredPrompt;
  state = {
    addApp: false
  };
  componentDidMount() {
    window.addEventListener("beforeinstallprompt", e => {
      e.preventDefault();
      this.deferredPrompt = e;
      this.setState({ addApp: true });
    });
  }

  showAppPrompt = () => {
    this.setState({ addApp: false });
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then(choiceResult => {
      if (choiceResult.outcome === "accepted") {
        console.log("User accepted the A2HS prompt");
      } else {
        console.log("User dismissed the A2HS prompt");
      }
      this.deferredPrompt = null;
    });
  };

  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={props => <HomePage />} />
          <Route
            exact
            path="/about/:id"
            component={props => (
              <Suspense fallback={<Loader />}>
                <LazyDetailsPage carID={props.match.params.id} />
              </Suspense>
            )}
          />
        </Switch>
        {this.state.addApp ? (
          <Prompt promptHandler={this.showAppPrompt} />
        ) : null}
        {/* <Prompt promptHandler={this.showAppPrompt}/> */}
      </div>
    );
  }
}
export default App;
