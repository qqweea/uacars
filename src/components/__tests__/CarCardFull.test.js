import React from "react";
import { shallow, mount } from "enzyme";
import ReactDOM from "react-dom";

import CarCardFull from "../CarCardFull";
import Error from '../Error'

describe("<CarCardFull />", () => {
  const wrapper = shallow(<CarCardFull />);
  const error = <Error />;

  it("renders without errors", () => {
    const div = document.createElement("div");
    ReactDOM.render(<CarCardFull />, div);
  });

  it("renders", () => {
    shallow(<CarCardFull />);
  });

  it("mounts", () => {
    mount(<CarCardFull />);
  });

  it("renders error with no props", () => {
    
    expect(wrapper).toContainReact(error);
  });

  it("works with props", ()=> {
    wrapper.setProps({car: {}});
    expect(wrapper).toContainReact(error);
  })
});
