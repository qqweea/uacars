import React from "react";
import { shallow, mount } from "enzyme";
import ReactDOM from "react-dom";

import CarCardSmall from "../CarCardSmall";
import Error from "../Error";

describe("<CarCardSmall />", () => {
  const wrapper = shallow(<CarCardSmall />);
  const error = <Error />;

  const carItem = {
    PERSON: "P",
    REG_ADDR_KOATUU: 8036100000,
    OPER_CODE: 70,
    OPER_NAME: "Реєстрацiя ТЗ привезеного з-за кордону по ВМД",
    D_REG: "22.01.2019",
    DEP_CODE: 12296,
    DEP: "ТСЦ 8047",
    BRAND: "FORD",
    MODEL: "MUSTANG",
    MAKE_YEAR: 2010,
    COLOR: "СІРИЙ",
    KIND: "ЛЕГКОВИЙ",
    BODY: "КУПЕ-B",
    PURPOSE: "ЗАГАЛЬНИЙ",
    FUEL: "БЕНЗИН",
    CAPACITY: 3700,
    OWN_WEIGHT: 1565,
    TOTAL_WEIGHT: 2041,
    N_REG: "АА5222ОС"
  };

  it("renders without errors", () => {
    const div = document.createElement("div");
    ReactDOM.render(<CarCardSmall />, div);
  });

  it("shallow renderes", () => {
    shallow(<CarCardSmall />);
  });

  it("mounts", () => {
    mount(<CarCardSmall />);
  });

  it("shows error with empty props", () => {
    wrapper.setProps({ carList: {} });
    expect(wrapper).toContainReact(error);
  });

  it("works with props", () => {
    wrapper.setProps({ carList: [carItem] });
    expect(wrapper.children().exists()).toBe(true);
    expect(wrapper.find('div')).toHaveLength(15);
  });
});
