import React, { Component } from "react";
import latinToCyrillic from '../helpers/latinToCyrillic'

import { connect } from "react-redux";
import styled from "styled-components";

export class SearchBar extends Component {
  state = {
    number: ""
  };
  submitForm = e => {
    console.log(latinToCyrillic(this.state.number));
    e.preventDefault();
  };

  handleChange = e => {
    const value = e.target.value;
    this.setState({ number: e.target.value });
    const pattern = /\w\w(\s?)\d{4}(\s?)\w\w/;
    if (pattern.test(value)) {
      e.target.classList.remove("notvalid");
      e.target.classList.add("valid");
    } else {
      e.target.classList.remove("valid");
      e.target.classList.add("notvalid");
    }
  };
  render() {
    return (
      <SearchForm onSubmit={this.submitForm}>
        <SearchInput
          id="search"
          required
          value={this.state.number}
          onChange={this.handleChange}
          placeholder="AA1234AA"
        />
        <Label htmlFor="search">Введіть гос. номер авто</Label>
        <Button type="submit">
          <i className="fas fa-search" />
        </Button>
      </SearchForm>
    );
  }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchBar);

const SearchForm = styled.form`
  position: sticky;
  top: 55px;
  z-index: 9;
  width: 100%;
  color: black;
  margin: 10px 0 0;
  overflow: hidden;
`;

const SearchInput = styled.input`
  border: none;
  background: none;
  background-color: white;
  font-size: inherit;
  width: 100%;
  height: 2em;
  border-radius: 5px;
  padding: 0 5px;
  &:focus,
  &:valid {
    & + label {
      transform: translate(-200%, -50%);
    }
  }

  &.notvalid {
    border: 2px solid red;
  }
  &.valid {
    border: 2px solid green;
  }
`;

const Label = styled.label`
  position: absolute;
  left: 5px;
  top: 50%;
  transform: translateY(-50%);
  z-index: 10;
  transition: 0.5s all;
  background-color: white;
`;

const Button = styled.button`
  border: none;
  background: none;
  margin: 0;
  height: 100%;
  padding: 2px 0.7em;
  position: absolute;
  right: 0;
  top: 0;
  i {
    font-size: 2em;
    margin: 0;
  }
`;
