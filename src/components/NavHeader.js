import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import Share from './Share'

export default function NavHeader(props) {
  const { carPlate = "" } = props;
  return (
    <Container>
      <div className="row no-gutters">
        {carPlate ? (
          <>
            <div className="col-4">
              <StyledLink to="/" alt="">
                <Header>
                  <i className="fas fa-chevron-left" /> Назад
                </Header>
              </StyledLink>
            </div>
            <div className="col-4">
              <Header className="center">{carPlate}</Header>
            </div>
            <div className="col-4">
              <Share licence={carPlate} />
            </div>
          </>
        ) : (
          <div className="col-12">
            <Header className="center">UA Cars</Header>
          </div>
        )}
      </div>
    </Container>
  );
}

const Container = styled.div`
  position: sticky;
  top: 0;
  z-index: 10;
  background-color: #424b5c;
  margin-bottom: 20px;
  padding: 8px 15px;
  `

const Header = styled.p`
  line-height: 2;
  margin: 0;
  font-size: 16px;

  &.smaller {
    font-size: 1.2em;
  }
  &.center {
    text-align: center;
    font-weight: bold;
  }
`;

const StyledLink = styled(Link)`
  color: white;
  text-decoration: none;
  line-height: 2;
  i {
    font-size: 2em;
    vertical-align: bottom;
  }
`;
