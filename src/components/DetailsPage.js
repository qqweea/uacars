import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import CarCardFull from './CarCardFull'
import SearchBar from './SearchBar'

export class DetailsPage extends Component {
  static propTypes = {
    carList: PropTypes.object,
    carID: PropTypes.string
  }
  
  render() {
    const {carsList, carID} = this.props;
    const localStorageCar = carsList.filter(car => car.N_REG === carID);
    const carObject = localStorageCar[0] || {};
    return (
        <CarCardFull car={carObject}>
          <SearchBar />
        </CarCardFull>
    )
  }
}

const mapStateToProps = (state) => ({
  carsList: state.globalAppState
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsPage)
