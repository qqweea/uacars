import React from 'react'
import {Link} from 'react-router-dom'

export default function Error() {
  return (
    <div>
      <br />
      <p>Помилка. Спробуйте знову</p>
      <Link style={{color: "white", textDecoration: "none"}} to="/">На головну</Link>
    </div>
  )
}
