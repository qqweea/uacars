import React, { useState, useEffect } from "react";
import {
  FacebookShareButton,
  TelegramShareButton,
  WhatsappShareButton,
  ViberShareButton,
  EmailShareButton
} from "react-share";
import styled from "styled-components";
import { withRouter } from "react-router";

export const Share = withRouter(props => {
  let textareaRef = React.createRef();
  let alertRef = React.createRef();

  const shareUrl = window.location.href;
  const shareTitle = `Технічні данні та характеристики по авто ${
    props.licence
  }`;
  const [visible, setVisible] = useState(false);

  const copyURl = () => {
    let textarea = textareaRef.current;
    let alert = alertRef.current;
    textarea.select();
    document.execCommand("copy");
    textarea.blur();
    alert.classList.add("d-block");
    setTimeout(() => {
      alert.classList.add("show");
    }, 1000);
    setTimeout(() => {
      alert.classList.remove("d-block", "show");
    }, 2000);
  };

  const hideSharePanel = () => {
    if (visible) {
      setVisible(false);
      window.removeEventListener("click", hideSharePanel, false);
    }
  };

  useEffect(() => {
    window.addEventListener("click", hideSharePanel, false);
  });
  return (
    <div style={{ textAlign: "right" }}>
      <i
        className="fas fa-share-alt-square"
        onClick={() => setVisible(!visible)}
        style={{ fontSize: "30px", textAlign: "right" }}
      />
      <Wrapper className={visible ? "visible" : ""}>
        <FacebookShareButton url={shareUrl} quote={shareTitle}>
          <i className="fab fa-facebook-square" />
        </FacebookShareButton>
        <TelegramShareButton url={shareUrl} title={shareTitle}>
          <i className="fab fa-telegram" />
        </TelegramShareButton>
        <WhatsappShareButton url={shareUrl} title={shareTitle}>
          <i className="fab fa-whatsapp-square" />
        </WhatsappShareButton>
        <ViberShareButton url={shareUrl} title={shareTitle}>
          <i className="fab fa-viber" />
        </ViberShareButton>
        <EmailShareButton url={shareUrl} title={shareTitle}>
          <i className="fas fa-envelope" />
        </EmailShareButton>
        <div onClick={copyURl}>
          <i className="fas fa-copy" />
          <CopyURL type="hidden" value={shareUrl} ref={textareaRef} />
        </div>
      </Wrapper>
      <Confirm ref={alertRef} id="confirm">
        Скопійовано
      </Confirm>
    </div>
  );
});

export default Share;

const Wrapper = styled.div`
  display: block;
  position: absolute;
  z-index: 20;
  overflow: hidden;
  right: -15px;
  top: 40px;
  background-color: #333;
  font-size: 34px;
  text-align: center;
  padding: 5px;
  border-radius: 5px;
  line-height: 1;
  transform: scaleY(0);
  transform-origin: top;
  opacity: 0;
  transition: 0.5s transform, 0.5s opacity;
  & i {
    margin: 5px;
  }
  &.visible {
    transform: scaleY(1);
    opacity: 1;
  }
`;

const CopyURL = styled.textarea`
  position: absolute;
  width: 0;
  height: 0;
  resize: none;
  border: none;
  opacity: 0;
  font-size: 18px;
  top: -200px;
`;

const Confirm = styled.div`
  position: absolute;
  top: 2px;
  left: -20px;
  margin: auto;
  background-color: #1f1f1f;
  padding: 2px 5px;
  border-radius: 5px;
  display: none;
  opacity: 1;
  &.show {
    transition: 1s opacity;
    opacity: 0;
  }
  &.d-block {
    display: block;
  }
`;
