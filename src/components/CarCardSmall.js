import React, { useState, useEffect } from "react";
import { capitalize } from "../helpers/helperFunctions";
import { Link } from "react-router-dom";
import styled, { css } from "styled-components";
import Error from "./ErrorMessage";

export default function CarCardSmall(props) {
  const { carList } = props;
  if (!carList || !Object.keys(carList).length) {
    return <Error />;
  }
  const [animated, setAnimated] = useState(false);
  useEffect(() => setAnimated(true));
  const links = carList.map((car, index) => (
    <div key={index} className="col-12 col-md-6">
      <Card animated={animated} delay={index * 100}>
        <CardLink to={`/about/${car.N_REG}`}>
          <div className="row">
            <div className="col-8">
              <Header>{capitalize(car.BRAND)}</Header>
              <Header>{capitalize(car.MODEL)}</Header>
              {car.MAKE_YEAR}

              <br />
            </div>
            <div className="col-4">
              <Logo
                src={`https://www.carlogos.org/logo/${capitalize(
                  car.BRAND
                )}-logo.png`}
                alt={capitalize(car.BRAND)}
              />
            </div>
          </div>
          <div className="row">
            <Line />
          </div>
          <div className="row">
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <TextSmall>Гос.номер</TextSmall>
                </div>
                <div className="col-12">{car.N_REG}</div>
              </div>
            </div>
            <div className="col-6">
              <div className="row">
                <div className="col-12">
                  <TextSmall>Колір</TextSmall>
                </div>
                <div className="col-12">{capitalize(car.COLOR)}</div>
              </div>
            </div>
          </div>
        </CardLink>
      </Card>
    </div>
  ));
  return <div className="row">{links}</div>;
}

const CardLink = styled(Link)`
  display: block;
  color: #fff;
  text-decoration: none;
  padding: 15px;
  border-radius: 10px;
`;

const Card = styled.div`
  background-color: #424b5c;
  border-radius: 10px;
  font-weight: 400;
  margin: 10px 0;
  transform: translateY(50px);
  opacity: 0;
  transition: 0.5s transform, 0.5s opacity;
  transition-delay: ${props => props.delay || 0}ms;
  ${props => props.animated && animatedCss}
`;

const animatedCss = css`
  transform: translateY(0);
  opacity: 1;
`;

const Logo = styled.img`
  max-width: 100%;
`;

const Header = styled.h3`
  font-size: 1.4em;
  font-weight: 700;
  margin: 0;
  line-height: 1;
`;
const TextSmall = styled.p`
  font-weight: 300;
  font-size: 0.9em;
  color: #c1c1c1;
  margin: 0;
  padding: 0;
`;

const Line = styled.hr`
  background-color: #333333;
  border: none;
  height: 1px;
  margin-top: 20px;
  width: 100%;
`;
