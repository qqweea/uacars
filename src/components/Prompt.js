import React, { useState } from "react";
import ReactDOM from "react-dom";
import styled from 'styled-components'

export default function Prompt(props) {
  const [open, setOpen] = useState(true);
  const {promptHandler} = props; 
  return ReactDOM.createPortal(
    <>
      {open ? (
        <PromptWindow>
          <div onClick = {promptHandler}>Додати на робочій стіл</div>
          <div onClick={()=> setOpen(false)}>Х</div>
        </PromptWindow>
      ) : null}
    </>,
    document.getElementById("root")
  );
}

const PromptWindow = styled.div`
  position: fixed;
  display: flex;
  flex: 1 auto;
  justify-content: space-between;
  padding: 10px 15px;
  width: 100%;
  bottom: 0;
  z-index: 999;
  background-color: #424b5c;
`
