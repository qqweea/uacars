import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import NavHeader from "./NavHeader";
import SearchBar from "./SearchBar";
import CarCardSmall from "./CarCardSmall";

export class HomePage extends Component {
  static propTypes = {
    carList: PropTypes.array
  };

  render() {
    const { carList = [] } = this.props;
    return (
      <>
        <NavHeader />
        <div className="container" style={{ minHeight: "200vh" }}>
          <h3 className="header">Шукати за номером автомобіля:</h3>
          <SearchBar />
          {Boolean(carList.length) && (
            <>
              <h3>Попередні запити:</h3>
              <CarCardSmall carList={carList} />
            </>
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  carList: state.globalAppState
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
