import React, { useEffect, useState } from "react";
import styled, { css } from "styled-components";
import { capitalize } from "../helpers/helperFunctions";
import NavHeader from "./NavHeader";
import Error from "./ErrorMessage";

export default function CarInfoCard(props) {
  const { car } = props;
  if (!car || !Object.keys(car).length) {
    return <Error />;
  }
  const carMake = capitalize(car.BRAND);
  const carModel = capitalize(car.MODEL);
  const carFuel = capitalize(car.FUEL);
  const carСolor = capitalize(car.COLOR);
  const carType = capitalize(car.KIND);
  const carLogo = `https://www.carlogos.org/logo/${carMake}-logo.png`;
  const [animated, setAnimated] = useState(false);
  useEffect(() => window.scrollTo(0, 0));
  useEffect(() => setAnimated(true));
  return (
    <>
      <NavHeader carPlate={car.N_REG} />
      <div className="container">
        {props.children}
        <Card animated={animated} className="container">
          <div className="row">
            <div className="col-6 d-flex">
              <div className="row align-self-center">
                <div className="col-12 d-flex">
                  <Header>{carMake}</Header>
                </div>
                <div className="col-12 d-flex">
                  <Header className="smaller">{carModel}</Header>
                </div>
                <div className="col-12 d-flex">{car.MAKE_YEAR} р.</div>
                <div className="col-12 d-flex" />
              </div>
            </div>
            <div className="col-6">
              <img src={carLogo} alt={carMake} style={{ maxWidth: "100%" }} />
            </div>
          </div>
          <div className="row">
            <div className="col">
              <Category>Технічні данні та характеристики</Category>
            </div>
          </div>
          <hr />
          <ScrollBar className="row flex-nowrap flex-md-wrap no-gutters">
            <div className="col">
              <div className="col-12">
                <i className="fas fa-brush" />
              </div>
              <div className="col-12">
                <TextSmall>Колір</TextSmall>
              </div>
              <div className="col-12">{carСolor}</div>
            </div>
            <div className="col">
              <div className="col-12">
                <i className="fas fa-car-side" />
              </div>
              <div className="col-12">
                <TextSmall>Тип</TextSmall>
              </div>
              <div className="col-12">{carType}</div>
            </div>
            <div className="col">
              <div className="col-12">
                <i className="fas fa-gas-pump" />
              </div>
              <div className="col-12">
                <TextSmall>Об'є́м</TextSmall>
              </div>
              <div className="col-12">{car.CAPACITY}</div>
            </div>
            <div className="col">
              <div className="col-12">
                <i className="fas fa-gas-pump" />
              </div>
              <div className="col-12">
                <TextSmall>Паливо</TextSmall>
              </div>
              <div className="col-12">{carFuel}</div>
            </div>
            <div className="col">
              <div className="col-12">
                <i className="fas fa-weight-hanging" />
              </div>
              <div className="col-12">
                <TextSmall>Маса</TextSmall>
              </div>
              <div className="col-12">
                {car.OWN_WEIGHT}/{car.TOTAL_WEIGHT}
              </div>
            </div>
            <div className="col">
              <div className="col-12">
                <i className="fas fa-info-circle" />
              </div>
              <div className="col-12">
                <TextSmall>Категорія</TextSmall>
              </div>
              <div className="col-12">{car.BODY}</div>
            </div>
          </ScrollBar>
          <hr />
          <div className="row">
            <div className="col-12">
              <Category>Дані про реєстрацію</Category>
            </div>
          </div>
          <div className="row align-items-center">
            <div className="col-4">
              <Category>Дата:</Category>
            </div>
            <div className="col-8">
              <Description>{car.D_REG}</Description>
            </div>
          </div>
          <div className="row align-items-center">
            <div className="col-4">
              <Category>Тип реєстрації:</Category>
            </div>
            <div className="col-8">
              <Description>{car.OPER_NAME}</Description>
            </div>
          </div>
          <div className="row align-items-center">
            <div className="col-4">
              <Category>Місце реєстрації:</Category>
            </div>
            <div className="col-8">
              <Description>{car.DEP}</Description>
            </div>
          </div>
        </Card>
      </div>
      <div style={{ height: "100vh" }} />
    </>
  );
}

const ScrollBar = styled.div`
  overflow-x: auto;
  text-align: center;
`;

const Header = styled.h3`
  font-size: 1.4em;
  font-weight: 700;
  margin: 0;
  line-height: 1;
  &.smaller {
    font-size: 1.2em;
  }
`;
const TextSmall = styled.p`
  font-weight: 300;
  font-size: 0.9em;
  color: #c1c1c1;
  margin: 0;
  padding: 0;
`;

const Category = styled.p`
  font-weight: 800;
  margin: 5px 0;
  &.smaller {
    font-weight: 400;
  }
`;

const Description = styled.p`
  margin: 0;
`;

const Card = styled.div`
  background-color: #424b5c;
  border-radius: 10px;
  /* padding: 15px; */
  font-weight: 400;
  margin: 10px 0;
  color: #fff;
  text-decoration: none;
  /* transform: scale(0); */
  opacity: 0;
  transform: scaleY(0.3);
  transform-origin: top;
  transition: 0.5s opacity, 0.5s transform;
  ${props => props.animated && animatedCss}
`;

const animatedCss = css`
  opacity: 1;
  transform: scaleY(1);
`;
