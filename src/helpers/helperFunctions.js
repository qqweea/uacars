export const capitalize = (inp) => {
  return inp && inp.length > 0
    ? inp.charAt(0).toUpperCase() + inp.slice(1).toLowerCase()
    : "";
}