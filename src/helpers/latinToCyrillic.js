const latinToCyrillic = inp => {
  let string = inp
    .toUpperCase()
    .replace(/A/g, "А")
    .replace(/B/g, "В")
    .replace(/C/g, "С")
    .replace(/E/g, "Е")
    .replace(/H/g, "Н")
    .replace(/I/g, "І")
    .replace(/K/g, "К")
    .replace(/M/g, "М")
    .replace(/O/g, "О")
    .replace(/P/g, "Р")
    .replace(/T/g, "Т")
    .replace(/X/g, "Х");
  return string;
};

export default latinToCyrillic;
