import * as TYPES from './actionTypes'

export const setAppStateAction = (payload) => dispatch => {
  dispatch({
    type: TYPES.SET_APP_STATE,
    payload: payload
  });
};
