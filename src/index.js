import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter as Router } from "react-router-dom";
import * as TYPES from "./actions/actionTypes";

import "./index.css";
import App from "./App";
import ErrorBoundry from "./components/ErrorBoundry";
import * as serviceWorker from "./serviceWorker";

const StoreObject = configureStore();
const store = StoreObject.store;
const persistor = StoreObject.persistor;

const carItem = {
  type: TYPES.SET_APP_STATE,
  payload: {
    PERSON: "P",
    REG_ADDR_KOATUU: 8036100000,
    OPER_CODE: 70,
    OPER_NAME: "Реєстрацiя ТЗ привезеного з-за кордону по ВМД",
    D_REG: "22.01.2019",
    DEP_CODE: 12296,
    DEP: "ТСЦ 8047",
    BRAND: "FORD",
    MODEL: "MUSTANG",
    MAKE_YEAR: 2010,
    COLOR: "СІРИЙ",
    KIND: "ЛЕГКОВИЙ",
    BODY: "КУПЕ-B",
    PURPOSE: "ЗАГАЛЬНИЙ",
    FUEL: "БЕНЗИН",
    CAPACITY: 3700,
    OWN_WEIGHT: 1565,
    TOTAL_WEIGHT: 2041,
    N_REG: "АА5222ОС"
  }
};

const carItem2 = {
  type: TYPES.SET_APP_STATE,
  payload: {
    PERSON: "P",
    REG_ADDR_KOATUU: 3222485901,
    OPER_CODE: 315,
    OPER_NAME: "Перереєстрація ТЗ на нов. власн. по договору укладеному в ТСЦ",
    D_REG: "10.01.2019",
    DEP_CODE: 12296,
    DEP: "ТСЦ 8047",
    BRAND: "KIA",
    MODEL: "CERATO",
    MAKE_YEAR: 2008,
    COLOR: "ЧЕРВОНИЙ",
    KIND: "ЛЕГКОВИЙ",
    BODY: "СЕДАН-B",
    PURPOSE: "ЗАГАЛЬНИЙ",
    FUEL: "БЕНЗИН",
    CAPACITY: 1591,
    OWN_WEIGHT: 1218,
    TOTAL_WEIGHT: 1650,
    N_REG: "АІ6061НТ"
  }
};

const carItem3 = {
  type: TYPES.SET_APP_STATE,
  payload: {
    PERSON: "P",
    REG_ADDR_KOATUU: 6320285201,
    OPER_CODE: 70,
    OPER_NAME: "Реєстрацiя ТЗ привезеного з-за кордону по ВМД",
    D_REG: "10.01.2019",
    DEP_CODE: 12356,
    DEP: "ТСЦ 6342",
    BRAND: "OPEL",
    MODEL: "VECTRA",
    MAKE_YEAR: 1997,
    COLOR: "ЧОРНИЙ",
    KIND: "ЛЕГКОВИЙ",
    BODY: "СЕДАН-B",
    PURPOSE: "ЗАГАЛЬНИЙ",
    FUEL: "БЕНЗИН АБО ГАЗ",
    CAPACITY: 1796,
    OWN_WEIGHT: 1050,
    TOTAL_WEIGHT: 1470,
    N_REG: "АХ4904НЕ"
  }
};

store.dispatch(carItem);
store.dispatch(carItem2);
store.dispatch(carItem3);
// persistor.purge();
// persistor.pause()

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Router>
        <ErrorBoundry>
          <App />
        </ErrorBoundry>
      </Router>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// if (location.hostname === "localhost" || location.hostname === "127.0.0.1")
//     alert("It's a local server!");
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
