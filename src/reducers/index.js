import { combineReducers } from 'redux';
import setAppStateReducer from './setAppStateReducer'

export default combineReducers({
  globalAppState: setAppStateReducer
});
