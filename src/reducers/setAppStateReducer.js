import * as TYPES from "../actions/actionTypes";

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case TYPES.SET_APP_STATE:
      return  [...state ,  {...action.payload}];
    default:
      return state;
  }
};
